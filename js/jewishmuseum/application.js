// Generated by CoffeeScript 1.3.3
(function() {
  var cstypo, fixSidebar, notifications, printHandler, protectEmails, protectImgs, togglable, videos;

  protectEmails = function() {
    return $('.em').filter('[data-em]').each(function() {
      var e, t;
      t = $(this);
      e = t.data('em').replace('/', '@');
      return t.attr('href', 'mailto:' + e).not('.custom-text').text(e);
    });
  };

  window.equalHeight = function(container) {
    return container.each(function() {
      var thisCont;
      thisCont = $(this);
      return thisCont.imagesLoaded(function() {
        return window.equalHeightDo(thisCont.children());
      });
    });
  };

  window.equalHeightDo = function(children) {
    var tallest;
    tallest = 0;
    children.each(function() {
      var th;
      th = $(this).height();
      if (tallest < th) {
        return tallest = th;
      }
    });
    children.height(tallest);
    return tallest;
  };

  togglable = function() {
    $('.toggle').on('click', function(e) {
      e.preventDefault();
      return $(this).toggleClass('in').next('.togglable').toggleClass('in');
    });
    return $('.togglable').on('click', '.close', function() {
      return $(this).parents('.togglable').toggleClass('in').prev('.toggle').toggleClass('in');
    });
  };

  fixSidebar = function() {
    var d;
    d = $('#detail');
    if (d.length === 1) {
      return d.css({
        minHeight: d.children('#detail-right').outerHeight() + 36
      });
    }
  };

  protectImgs = function() {
    return $('#tab-images').on('contextmenu', 'img', function(e) {
      e.preventDefault();
      return false;
    });
  };

  notifications = function() {
    var n;
    n = $('#notification-src');
    if (n.length === 1) {
      $('body').on('click', '.ui-widget-overlay', function() {
        return $('.ui-dialog-titlebar-close').trigger('click');
      });
      return n.dialog({
        modal: true,
        width: 335,
        draggable: false,
        resizable: false,
        minHeight: 0,
        open: function(event, ui) {
          console.log($(this).parents('.ui-dialog'));
          return $(this).parents('.ui-dialog').css({
            left: '50%',
            marginLeft: -335 / 2
          });
        }
      });
    }
  };

  videos = function() {
    var resizeRatio, swfLocation, wrap;
    swfLocation = "" + window.baseUrl + "/viewers/apps/flowplayer-3.2.7.swf";
    wrap = $('#detail-left');
    resizeRatio = function(el, wrap) {
      var r;
      r = parseFloat(el.data('ratio'));
      return el.css({
        display: 'block',
        width: wrap.width(),
        height: wrap.width() / r
      });
    };
    $('.video-me').each(function() {
      var id, t;
      t = $(this);
      id = t.attr('id');
      if (t.is('.ratio-resize[data-ratio]')) {
        resizeRatio(t, wrap);
      }
      return flowplayer(id, swfLocation);
    });
    return $(window).on('resize', function() {
      return $('.ratio-resize').filter('[data-ratio]').each(function() {
        return resizeRatio($(this), wrap);
      });
    });
  };

  printHandler = function() {
    return window.onbeforeprint = function() {
      return alert('print!');
    };
  };

  cstypo = function() {
    if ($('html').hasClass('lang-cs')) {
      return $('p, li, div').add('.footer-left a').cstypo();
    }
  };

  $(function() {
    $(window).on('load', function() {
      return $(window).trigger('resize');
    });
    window.equalHeight($('.equal-height, .grid > ul'));
    fixSidebar();
    cstypo();
    notifications();
    protectImgs();
    togglable();
    videos();
    protectEmails();
    return printHandler();
  });

}).call(this);
