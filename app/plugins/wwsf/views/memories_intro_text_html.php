<?php
global $g_ui_locale;

	if ($g_ui_locale == 'en_US') {
?>
		Eyewitnesses take a look back in time. In MEMORIES, they tell the stories behind their photos and films and allow insights into their personal experiences during this momentous time.
		<br/><br/><?php print caNavLink($this->request, "Join in!", "", "", "About", "participate"); ?> Send us your photos and add your stories and memories to our collection.
		<br/><br/>
<?php
	}else{
?>
		Zeitzeugen blicken zurück: In den ERINNERUNGEN erzählen sie die Entstehungsgeschichten ihrer Fotos und Filme und gewähren Einblicke in persönliche Erlebnisse während dieser historischen Zeit.
		<br/><br/><?php print caNavLink($this->request, "Machen Sie mit!", "", "", "About", "participate"); ?>  Schicken Sie uns Ihre Bilder und erweitern Sie unsere Sammlung durch Ihre Geschichten und Erinnerungen.
		<br/><br/>
<?php			
	}	
?>