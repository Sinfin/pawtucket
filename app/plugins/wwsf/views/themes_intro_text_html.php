<?php
global $g_ui_locale;

			if ($g_ui_locale == 'en_US') {
?>
				Under THEMES you will find albums compiled by the website editors to fit specific subjects such as unexpected events, historical stations, persons and motives of the time. Here you can also take a glimpse into the Deutsche Kinemathek's exhibition of the same name: The 6 chapters of private records forming the focus of the exhibition are here presented with the label “Exhibition themes”.
<?php
			}else{
?>
				In den THEMEN finden Sie von der Redaktion zusammengestellte Alben zu kennzeichnenden wie auch unvermuteten Ereignissen, Stationen, Personen und Motiven der Zeit. Hier können Sie auch einen Blick in die gleichnamige Ausstellung der Deutschen Kinemathek werfen: Die 6 Kapitel mit privaten Aufnahmen, die hier den Schwerpunkt bilden, präsentieren wir in den Alben “Ausstellungsthemen”.
<?php			
			}
?>