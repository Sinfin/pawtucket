<?php
	$va_set_list 			= $this->getVar('sets');
	$va_set_display_items 	= $this->getVar('set_display_items');

?>
	<div id="navSelectorFeatures"><img src='<?php print $this->request->getThemeUrlPath(); ?>/graphics/navSelector.png' width='25' height='14' border='0'></div><!-- end navSelectorFeatures -->

	<!-- BEGIN List of sets -->
	<div id="scrollableContainer">
	<a class="prevPage browse left featuresTileScrollControl"><img src="<?php print $this->request->getThemeUrlPath(); ?>/graphics/featuresArrowLeft.gif" width="16" height="32" border="0"></a>
	<!-- root element for scrollable --> 
	<div class="scrollable">     
		<!-- root element for the items --> 
		<div class="items"> 
<?php
		$t_set = new ca_sets();
		foreach($va_set_list as $va_set) {
			$vn_set_id = $va_set['set_id'];
			if ($t_set->load($vn_set_id)) {
				$va_item = $va_set_display_items[$vn_set_id][array_shift(array_keys($va_set_display_items[$vn_set_id]))];
?>
				<div class='featuresTile'>
					<div class='featuresTileImage'>
						<div><a href='#' onclick='loadSet(<?php print $va_set['set_id']; ?>);  return false;'><?php print $va_item['representation_tag']; ?></a></div>
					</div>
					<div class='featuresTileTitle'>
						<?php print (strlen($t_set->getLabelForDisplay()) > 30) ? substr($t_set->getLabelForDisplay(), 0, 30)."..." : $t_set->getLabelForDisplay(); ?>
					</div>
				</div>
<?php
			}
		}
?>
			<div class='featuresTileEmpty'>
				<!-- empty -->
			</div>
		</div><!-- end items -->
	</div><!-- end scrollable -->	
 	<a class="nextPage browse right featuresTileScrollControl"><img src="<?php print $this->request->getThemeUrlPath(); ?>/graphics/featuresArrowRight.gif" width="16" height="32" border="0"></a>
 	<div style="clear: both; height:1px;"><!-- empty --></div>
	</div><!-- end scrollableContainer --><!-- END List of sets -->
	<div id="featuresText">
		The "Features" sections of the Archive are curated selections of images, documents, audio and video from the database chronicling the history of the New Museum. 
	</div><!-- end featuresText --><div style="height:10px;"></div></div><!-- end header --></div><!-- end headerBg -->
<div id="pageBg">
	<div id="featuresContent">
	</div><!-- end featuresContent -->
<div id="footerLogo"><img src='<?php print $this->request->getThemeUrlPath(); ?>/graphics/footerLogo.gif' width='149' height='85' border='0'></div><!-- end footerLogo --><div style="clear:both; height:1px;"><!-- empty --></div></div><!-- end pageBg-->

	<script type="text/javascript"> 
		jQuery(document).ready(function() { 
			jQuery("div.scrollable").scrollable(); 
		 
		}); 
	
		function loadSet(set_id) {
			jQuery('#featuresContent').load('<?php print caNavUrl($this->request, 'Features', 'Show', 'displaySet'); ?>', {set_id: set_id});
		}
	</script>