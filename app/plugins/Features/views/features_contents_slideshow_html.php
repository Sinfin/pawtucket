<?php
	$t_set = $this->getVar('t_set');
	$va_items = caExtractValuesByUserLocale($t_set->getItems(array('thumbnailVersion' => 'medium', 'returnItemAttributes' => array('set_item_description'), 'checkAccess' => $this->getVar('access_values'))));
?>
	<div id="featuresPageText">
		<div id="featuresTitle"><?php print $t_set->getLabelForDisplay(); ?></div>
		<div id="featuresDescription"><?php print $t_set->getSimpleAttributeValue('set_introduction'); ?></div>
	</div><!-- end featuresPageText -->
	<div id="featuresSlideshowArea">

	<table>
		<tr valign="top">
			<td class='featuresSlideshowScrollControl'><div class="featuresSlideshowScrollControlLink"><a href="#" onclick="featuresSlideshowScroller.scrollToPreviousImage(); return false;" class="featuresSlideshowScrollControlLink"><img src="<?php print $this->request->getThemeUrlPath(); ?>/graphics/arrowFeaturesSlideshowLeft.gif" width="16" height="32" border="0"></a></div></td>
			<td>
				<div id="featuresSlideshow">
					<div id="featuresSlideshowScrollingViewer">
						<div id="featuresSlideshowScrollingViewerContainer">
							<div id="featuresSlideshowScrollingViewerImageContainer"></div>
						</div>
					</div>
					<br/>
					<div id='featuresSlidehowScrollingViewerItemText'><!-- empty --></div>
				</div>
			</td>
			<td class='featuresSlideshowScrollControl'><div class="featuresSlideshowScrollControlLink"><a href="#" onclick="featuresSlideshowScroller.scrollToNextImage(); return false;" class="featuresSlideshowScrollControlLink"><img src="<?php print $this->request->getThemeUrlPath(); ?>/graphics/arrowFeaturesSlideshowRight.gif" width="16" height="32" border="0"></a></div></td>
		</tr>
	</table></div><!-- end featuresSlideshowArea -->

<script type="text/javascript">
<?php
	$va_imgs = array();
	foreach($va_items as $va_item) {
		$vs_description = "";
		if($va_item['ca_attribute_set_item_description']){
			$vs_description = $va_item['ca_attribute_set_item_description']."<br><br>";
      }
		$vs_description .= caNavLink($this->request, _t($va_item['name']), "", "Detail", "Object", "Show", array("object_id" => $va_item["row_id"]));


		$va_imgs[] = "{url:'".$va_item['representation_url']."', width: ".$va_item['representation_width'].", height: ".
		$va_item['representation_height'].", link: '". __CA_URL_ROOT__ ."/index.php/Detail/Object/Show/object_id/" . $va_item['row_id'] ." ', title: '".addslashes($vs_description)."'}";
	}
?>
	var featuresSlideshowScroller = caUI.initImageScroller([<?php print join(",", $va_imgs); ?>], 'featuresSlideshowScrollingViewerImageContainer', {
		containerWidth: 525, containerHeight: 500,
		imageCounterID: 'featuresSlideshowScrollingViewerCounter',
		scrollingImageClass: 'featuresSlideshowScrollerImage',
		scrollingImagePrefixID: 'featuresSlideshow',
		imageTitleID: 'featuresSlidehowScrollingViewerItemText',
		dontUseEffects: true
	});
</script>