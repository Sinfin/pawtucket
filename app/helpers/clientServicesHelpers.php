<?php
/** ---------------------------------------------------------------------
 * app/helpers/clientServicesHelpers.php : miscellaneous functions
 * ----------------------------------------------------------------------
 * CollectiveAccess
 * Open-source collections management software
 * ----------------------------------------------------------------------
 *
 * Software by Whirl-i-Gig (http://www.whirl-i-gig.com)
 * Copyright 2011 Whirl-i-Gig
 *
 * For more information visit http://www.CollectiveAccess.org
 *
 * This program is free software; you may redistribute it and/or modify it under
 * the terms of the provided license as published by Whirl-i-Gig
 *
 * CollectiveAccess is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTIES whatsoever, including any implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * This source code is free and modifiable under the terms of
 * GNU General Public License. (http://www.gnu.org/copyleft/gpl.html). See
 * the "license.txt" file for details, or visit the CollectiveAccess web site at
 * http://www.CollectiveAccess.org
 * 
 * @package CollectiveAccess
 * @subpackage utils
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License version 3
 * 
 * ----------------------------------------------------------------------
 */

 /**
   *
   */
	$g_caClientServicesNameCache = array(); 


	# ---------------------------------------
	/**
	 * Returns client services configuration
	 *
	 * @return Configuration Returns reference to client services configuration
	 */
	function caGetClientServicesConfiguration() {
		$o_config = Configuration::load();
 		return Configuration::load($o_config->get('client_services_config'));
	}
	# ---------------------------------------
	/**
	 * Formats communication for display in messages list
	 *
	 * @param RequestHTTP $po_request
	 * @param array $pa_data
	 * @param array $pa_options
	 *		viewContentDivID = 
	 *		additionalMessages =
	 *		isAdditionalMessage =
	 *
	 * @return string 
	 */
	function caClientServicesFormatMessageSummary($po_request, $pa_data, $pa_options=null) {
		$vb_is_additional_message = (bool)(isset($pa_options['isAdditionalMessage']) && $pa_options['isAdditionalMessage']);
		$vb_is_unread = !(bool)$pa_data['read_on'];
	
		if ($vb_is_additional_message) {
			$vs_class = ($vb_is_unread) ? "caClientCommunicationsAdditionalMessageSummary caClientCommunicationsMessageSummaryUnread" : "caClientCommunicationsAdditionalMessageSummary";
			$vs_buf = "<div class='{$vs_class}' id='caClientCommunicationsMessage_".$pa_data['communication_id']."'>";
		} else {
			$vs_class = ($vb_is_unread) ? "caClientCommunicationsMessageSummary caClientCommunicationsMessageSummaryUnread" : "caClientCommunicationsMessageSummary";
			$vs_buf = "<div class='{$vs_class}'>";
		}
		
		$vs_unread_class = ($vb_is_unread) ? "caClientCommunicationsMessageSummaryUnread" : "";
		
		$vs_buf .= "<div class='caClientCommunicationsMessageSummaryContainer' id='caClientCommunicationsMessage_".$pa_data['communication_id']."'>";
		$vs_buf .= "<div class='caClientCommunicationsMessageSummaryFrom {$vs_unread_class}'>".caClientServicesGetSenderName($pa_data)."</div>";
		
		$vs_buf .= "<div class='caClientCommunicationsMessageSummaryDate {$vs_unread_class}'>".caGetLocalizedDate($pa_data['created_on'], array('dateFormat' => 'delimited'))."</div>";
		$vs_buf .= "<div class='caClientCommunicationsMessageSummarySubject {$vs_unread_class}'>".$pa_data['subject']."</div>";
		
		
		$vn_num_additional_messages = is_array($pa_options['additionalMessages']) ? sizeof($pa_options['additionalMessages']) : 0;
		
		if ($vn_num_additional_messages) {
			$vs_buf .= "<div class='caClientCommunicationsMessageSummaryCounter' id='caClientCommunicationsMessageAdditionalCount".$pa_data['communication_id']."'><a href='#' onclick='jQuery(\"#caClientCommunicationsMessageAdditional".$pa_data['communication_id']."\").slideToggle(250);' class='caClientCommunicationsMessageSummaryCounter'>{$vn_num_additional_messages}</a></div>";
		}
		
		$vs_buf .= "<div class='caClientCommunicationsMessageSummaryText'>".mb_substr($pa_data['message'], 0, 100)."</div>";
		$vs_buf .= "</div>";
		
		if ($vn_num_additional_messages) {
			$vs_buf .= "<div class='caClientCommunicationsMessageAdditional' id='caClientCommunicationsMessageAdditional".$pa_data['communication_id']."'>";
			$pa_additional_options = $pa_options;
			unset($pa_additional_options['additionalMessages']);
			$pa_additional_options['isAdditionalMessage'] = true;
			foreach($pa_options['additionalMessages'] as $va_additional_message) {
				$vs_buf .= caClientServicesFormatMessageSummary($po_request, $va_additional_message, $pa_additional_options);
			}
			$vs_buf .= "</div>";
		}
		
		$vs_buf .= "</div>\n";
		return $vs_buf;
	}
	# ---------------------------------------
	/**
	 * Formats communication for display in message window
	 *
	 * @param RequestHTTP $po_request
	 * @param array $pa_data
	 * @param array $pa_options
	 *		viewContentDivID = 
	 *		replyButton = 
	 *
	 * @return string 
	 */
	function caClientServicesFormatMessage($po_request, $pa_data, $pa_options=null) {
		$vs_buf = "<div class='caClientCommunicationsMessage'>";
		$vs_buf .= "<div class='caClientCommunicationsMessageDate'>".caGetLocalizedDate($pa_data['created_on'], array('dateFormat' => 'delimited'))."</div>";
		
		if (isset($pa_options['replyButton']) && $pa_options['replyButton']) {
			$vs_buf .= "<div style='float: right; clear: both;'>".$pa_options['replyButton']."</div>";
		}
		$vs_buf .= "<div class='caClientCommunicationsMessageFrom'>"._t('From').": ".caClientServicesGetSenderName($pa_data)."</div>";
		$vs_buf .= "<div class='caClientCommunicationsMessageSubject'>"._t('Subject').": ".$pa_data['subject']."</div>";
		
		$vs_buf .= "<div class='caClientCommunicationsMessageText'>".$pa_data['message']."</div>";
		$vs_buf .= "</div>\n";
		return $vs_buf;
	}
	# ---------------------------------------
	/**
	 * 
	 *
	 * @param array $pa_data
	 * @param array $pa_options
	 *
	 * @return string 
	 */
	function caClientServicesGetSenderName($pa_data, $pa_options=null) {
		global $g_caClientServicesNameCache;
		if (!isset($g_caClientServicesNameCache[$pa_data['from_user_id']])) {
			$t_user = new ca_users($pa_data['from_user_id']);
			return $g_caClientServicesNameCache[$pa_data['from_user_id']] = $t_user->get('fname').' '.$t_user->get('lname');
		} else {
			return $g_caClientServicesNameCache[$pa_data['from_user_id']];
		}
	}
	# ---------------------------------------
?>